/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixapplication;

import java.util.Random;

/**
 *
 * @author Наталья
 */

/*
  Поиск прямоугольного фрагмента с максимальной суммой в двухмерной матрице.
*/
public class Matrix {
    private final int[][] matr;
    private final int rows;
    private final int columns;
    int maxSum, top, left, bottom, right;
    
    public Matrix(int n, int m, int leftBorder, int rightBorder) {
        columns = m;
        rows = n;
        
        matr = new int[n][m];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                matr[i][j] = randInt(leftBorder, rightBorder);
        }
    }
    
    private int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
    private int max(int a, int b) {
        if (a > b) return a;
        return b;
    }
    
    public int[][] getMatrix() {
        return matr;
    }
    
    public int getLeft() {
        return left;
    }
    
    public int getRight() {
        return right;
    }
    
    public int getTop() {
        return top;
    }
    
    public int getBottom() {
        return bottom;
    }
    
    public int getMaxSum() {
        return maxSum;
    }
    
    public void solve() {
        int[][] vps = new int[rows][columns];
        for (int j = 0; j < columns; j++) {
            vps[0][j] = matr[0][j];
            for (int i = 1; i < rows; i++)
                vps[i][j] = vps[i-1][j] + matr[i][j];
        }
        
        int[] sum = new int[columns];
        int[] pos = new int[columns];
        
        maxSum = matr[0][0];
        left = right = bottom = top = 0;
        
        for (int i = 0; i < rows; i++) {
            for (int k = i; k < rows; k++) {
                for (int j = 0; j < columns; j++) {
                    sum[j] = 0;
                    pos[j] = 0;
                }
                int localMax = 0;
                sum[0] = vps[k][0] - (i==0 ? 0 : vps[i-1][0]);
                for (int j = 1; j < columns; j++) {
                    int value = vps[k][j] - (i==0 ? 0 : vps[i-1][j]);
                    
                    if (sum[j-1] > 0) {
                        sum[j] = sum[j-1] + value;
                        pos[j] = pos[j-1];
                    } else {
                        sum[j] = value;
                        pos[j] = j;
                    }
                    if (sum[j] > sum[localMax])
                        localMax = j;
                }
                
                if (sum[localMax] > maxSum) {
                    maxSum = sum[localMax];
                    top = i;
                    left = pos[localMax];
                    bottom = k;
                    right = localMax;
                }
            }
        }
    }
}
